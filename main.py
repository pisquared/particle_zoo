from particles import Particle, Neutron, Proton, Electron, ElectronNeutrino


# type hint, particle will be from type Particle, returns boolean
def does_obey_Bose_Einstein(particle: Particle) -> bool:
    return int(particle.spin) == particle.spin


def main():
    neutron = Neutron()
    result = neutron.beta_decay()
    assert len(result) == 3
    assert isinstance(result[0], Proton)
    assert isinstance(result[1], Electron)
    assert isinstance(result[2], ElectronNeutrino)
    assert result[2].is_matter is False
    assert does_obey_Bose_Einstein(neutron) is False


if __name__ == "__main__":
    main()
