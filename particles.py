from constants import MASS_NEUTRINO, MASS_ELECTRON, MASS_NEUTRON, MASS_PROTON, ELEMENTARY_CHARGE, QUARK_BARYON_NUMBER


class Particle(object):
    def __init__(self, charge, mass, spin, is_matter=True):
        self.charge = charge
        self.mass = mass
        self.spin = spin
        self.is_matter = is_matter


class Quark(Particle):
    # 0.0 because needs to be the same data type
    def __init__(self, isospin=0.0, isospin_z=0.0, strangeness=0.0, charmness=0.0, bottomness=0.0, topness=0.0, *args,
                 **kwargs):
        super().__init__(*args, **kwargs)  # charge, mass, spin come from the class Particle
        self.isospin = isospin
        self.isospin_z = isospin_z
        self.strangeness = strangeness
        self.charmness = charmness
        self.bottomness = bottomness
        self.topness = topness
        self.baryon_number = QUARK_BARYON_NUMBER
        self.hypercharge = self.baryon_number + self.strangeness + self.charmness + self.bottomness + self.topness


class Up(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(isospin=0.5, isospin_z=0.5, charge=2 / 3, *args, **kwargs)


class Down(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(isospin=0.5, isospin_z=-0.5, charge=-1 / 3, *args, **kwargs)


class Strange(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(strangeness=-1, charge=-1 / 3, *args, **kwargs)


class Charm(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(charmness=1, charge=2 / 3, *args, **kwargs)


class Bottom(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(bottomness=-1, charge=-1 / 3, *args, **kwargs)


class Top(Quark):
    def __init__(self, *args, **kwargs):
        super().__init__(topness=1, charge=2 / 3, *args, **kwargs)


class Lepton(Particle):
    """
    Lepton inherits from Particles; fundamental;
    includes electron, muon, tao, el. neutrino, muon neutrino, tao neutrino
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, spin=0.5, **kwargs)  # super means re-defining the method from the inherited class


class Hadron(Particle):
    """
    Lepton inherits from Particles; NOT fundamental (consist of quarks);
    includes Baryons (proton, neutron) and Mesons (Ka, Pi, ..)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Baryon(Hadron):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, spin=0.5, **kwargs)


class Neutron(Baryon):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, charge=0, mass=MASS_NEUTRON, **kwargs)

    def beta_decay(self):
        return [Proton(), Electron(), ElectronNeutrino(is_matter=False)]


class Proton(Baryon):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, charge=ELEMENTARY_CHARGE, mass=MASS_PROTON, **kwargs)


class Electron(Lepton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, charge=-ELEMENTARY_CHARGE, mass=MASS_ELECTRON, **kwargs)


class ElectronNeutrino(Lepton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, charge=0, mass=MASS_NEUTRINO, **kwargs)
